import { Injectable } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LearningService {
  public emitEvent = new EventEmitter();
  private list: string[] = ['Machado', 'Faca', 'Adaga', 'Espada', 'Arco']

  constructor() { }

  public get getList(): string[] {

    return this.list;
  }

  public acionarSubscribe(): void {
    this.emitEvent.emit(this.list);
  }
}
