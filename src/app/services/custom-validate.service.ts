import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})

export class CustomValidateService {

  constructor() { }

  static customValidate(): ValidatorFn {
    const verification = (control: AbstractControl): ValidationErrors | null => {
      // Lógica de validação
      if (!control.value.includes('jogador')) {
        return { customValidate: false }
      } else { return null };
    };
    return verification;
  }

  // Veriricação que recebe um RegEx.
  static validateRegex(nameRe: RegExp): ValidatorFn {
    const verification = (control: AbstractControl): ValidationErrors | null => {
      if (!nameRe.test(control.value)) {
        return { forbiddenName: { value: control.value } };
      } else { return null };
    };
    return verification;
  }

}
