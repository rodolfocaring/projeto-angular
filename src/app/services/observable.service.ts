import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

interface ICharacter {
  name: string;
  age: string;
  house: string;
}

@Injectable({
  providedIn: 'root'
})
export class ObservableService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'access_token'
    })
  }

  constructor(private httpClient: HttpClient) {

  }

  public getCharacters(): Observable<any> {
    const result = this.httpClient.get<any>(`${environment.url}/characters/students`).pipe((res) => res, (error) => error);
    return result;
  }

  public postCharacters(value: ICharacter): Observable<ICharacter> {
    const result = this.httpClient.post<ICharacter>(`${environment.url}/characters`, value, this.httpOptions).pipe((res) => res , (error) => error)
    return result;
  }
}
