import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.scss']
})
export class OutputComponent implements OnInit {
  @Output() infoIssued = new EventEmitter();
  products: { name: string, price: number}[] =  [
    { name: 'Camisa', price: 23 },
    { name: 'Calça', price: 30 },
    { name: 'Bermuda', price: 25 },
  ];

  constructor() { }

  ngOnInit(): void {
  }

  sendProduct(productSelected: { name: string, price: number}): void {
    this.infoIssued.emit(productSelected);
  }
}
