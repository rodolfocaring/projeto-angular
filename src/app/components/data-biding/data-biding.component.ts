import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-biding',
  templateUrl: './data-biding.component.html',
  styleUrls: ['./data-biding.component.scss']
})
export class DataBidingComponent implements OnInit {
  public name: string = 'José Rodolfo'
  public statusButton: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  public sendAlert(event: Event): void {
    console.log(event)
    alert(`Olá, ${this.name}. Boas vindas!`)
  }

}
