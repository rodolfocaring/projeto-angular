import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diretivas',
  templateUrl: './diretivas.component.html',
  styleUrls: ['./diretivas.component.scss']
})
export class DiretivasComponent implements OnInit {
  public isShow: boolean = true;
  public rugbyStaterPack: string[] = ['Chuteira', 'Meião', 'Short', 'Camisa', 'Vontade', 'Respeito'];
  public items: string[] = [];
  public name: string= '';

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.items = ['Arroz', 'Feijão', 'Carne', 'Tomate']
    }, 10000);
  }

  public deleteElement(i: number): void {
    this.rugbyStaterPack.splice(i, 1);
  }

  setInfo(i: any): void {
    console.log("ae")
  }
}
