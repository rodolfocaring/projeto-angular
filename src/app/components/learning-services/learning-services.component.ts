import { Component, OnInit } from '@angular/core';
import { LearningService } from 'src/app/services/learning.service';

@Component({
  selector: 'app-learning-services',
  templateUrl: './learning-services.component.html',
  styleUrls: ['./learning-services.component.scss']
})
export class LearningServicesComponent implements OnInit {
  public itensList: string[] = [];

  constructor(private learningService: LearningService) { }

  ngOnInit(): void {
    this.itensList = this.learningService.getList;
  }

  acionar(): void {
    this.learningService.acionarSubscribe();
  }

}
