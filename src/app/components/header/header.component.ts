import { Component, Output, OnInit } from '@angular/core';
import { LearningService } from 'src/app/services/learning.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() public user: string = "rodolfo@joganada.com.br";

  constructor(private learningService: LearningService) { }

  ngOnInit(): void {
    this.learningService.emitEvent.subscribe(
      (res) => { alert('Subscribed!'); console.log(res) });
  }
}