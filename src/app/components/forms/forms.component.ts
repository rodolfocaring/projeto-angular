import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  public jogador: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  submitForm(form: NgForm): void {
    if (form.valid) {
      console.log(form.form);
    }
  }

}
