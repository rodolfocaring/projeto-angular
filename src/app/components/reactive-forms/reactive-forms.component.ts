import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidateService } from 'src/app/services/custom-validate.service';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.scss']
})
export class ReactiveFormsComponent implements OnInit {
  public cadastroForm: FormGroup = this.fb.group({
    nome: ['', Validators.required],
    sobrenome: ['', [Validators.required, Validators.minLength(10), CustomValidateService.validateRegex(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+$/)]],
  })

  constructor(private fb: FormBuilder,) { }

  ngOnInit(): void {
  }

  submitForm(): void {
    if (this.cadastroForm.valid) {
      console.log(this.cadastroForm.value);
    }
  }
}
