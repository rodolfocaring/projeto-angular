import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.scss']
})

export class PipesComponent implements OnInit {
  public date: Date = new Date();
  public objectTest: { name: string, age: number}[] = [
    { name: 'José Rodolfo', age: 30},
    { name: 'Lis Valério', age: 4 },
    { name: 'Leiliane Clélia', age: 34}
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
