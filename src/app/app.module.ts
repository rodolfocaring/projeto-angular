import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { DataBidingComponent } from './components/data-biding/data-biding.component';
import { FormsModule } from '@angular/forms';
import { DiretivasComponent } from './components/diretivas/diretivas.component';
import { PipesComponent } from './components/pipes/pipes.component';
import { SharedModule } from './shared/shared.module';
import { LearningServicesComponent } from './components/learning-services/learning-services.component';
import { HttpClientModule } from '@angular/common/http';
import { CharactersHPComponent } from './pages/characters-hp/characters-hp.component';
import { FormsComponent } from './components/forms/forms.component';
import { ReactiveFormsComponent } from './components/reactive-forms/reactive-forms.component';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    DataBidingComponent,
    DiretivasComponent,
    PipesComponent,
    LearningServicesComponent,
    CharactersHPComponent,
    FormsComponent,
    ReactiveFormsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    SharedModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
