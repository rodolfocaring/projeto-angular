import { Component, DoCheck, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit, OnChanges, DoCheck {
  public title: string = 'Joga Nada';
  public numero: number = 0;
  @Input() public user: string = '';

  constructor() { }

  public addNumber(): number {
    return this.numero++;
  }

  ngOnInit(): void {
    setInterval(() => {
      this.numero = this.numero + 1;
      console.log(this.numero);
    }, 5000);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes['title']);
    console.log("O input foi alterado.");
  }

  ngDoCheck(): void {
    console.log("DoCheck atualizado.")
  }

  setInfo(i: any): void {
    console.log(i)
  }
}
