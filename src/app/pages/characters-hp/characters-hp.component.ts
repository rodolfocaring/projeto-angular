import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ObservableService } from 'src/app/services/observable.service';

@Component({
  selector: 'app-characters-hp',
  templateUrl: './characters-hp.component.html',
  styleUrls: ['./characters-hp.component.scss']
})
export class CharactersHPComponent implements OnInit {

  constructor(private observableService: ObservableService) { }
  public allCharacters: any = '';

  ngOnInit(): void {
    this.getCharecters();
  }

  getCharecters() {
    return this.observableService.getCharacters().subscribe({
      next: (res) => this.allCharacters = res,
      error: (error) => console.log(error)
    });
  }

}
